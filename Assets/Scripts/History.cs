// Script for tracking movement history.

using System.Collections.Generic;
using UnityEngine;

public class History : MonoBehaviour
{
	#region Events
	public delegate void VoidNoArgs();
	public event VoidNoArgs OnUndo;
	#endregion


	#region Inspector
	public Character Player;
	public Character Minotaur;
	#endregion


	#region Private fields
	struct Snapshot
	{
		public Vector2Int player;
		public Vector2Int minotaur;
	}

	List<Snapshot> history = new List<Snapshot>();
	#endregion


	private void Start()
	{
		// Snapshots are taken when the player stops.
		Player.OnStopEvent += Record;
	}


	#region History
	/// <summary>
	/// Takes a snapshot if there was any change.
	/// </summary>
	public void Record()
	{
		if (history.Count == 0)
		{
			TakeSnapshot();
		}
		else
		{
			var last = history[history.Count - 1];
			if (Player.position != last.player || Minotaur.position != last.minotaur)
				TakeSnapshot();
		}
	}


	/// <summary>
	/// Wipes history.
	/// </summary>
	public void Wipe()
	{
		history.Clear();
	}


	/// <summary>
	/// Reverts characters positions one stap back.
	/// </summary>
	public void Undo()
	{
		if (history.Count > 1)
		{
			history.RemoveAt(history.Count - 1);
			var last = history[history.Count - 1];
			Player.SetPosition(last.player);
			Minotaur.SetPosition(last.minotaur);
			OnUndo?.Invoke();
		}
	}


	/// <summary>
	/// Creates a snapshot of current character positions.
	/// </summary>
	private void TakeSnapshot()
	{
		Snapshot snapshot;
		snapshot.player = Player.position;
		snapshot.minotaur = Minotaur.position;
		history.Add(snapshot);
	}
	#endregion
}
