// Player controller.

using UnityEngine;

public class Player : Character
{
	#region Inspector
	public Minotaur minotaur;
	#endregion


	#region Properties
	/// <summary>
	/// Whether the player is currently standing at the exit.
	/// </summary>
	public bool AtExit
	{
		get { return level.GetTile(position.x, position.y).Exit; }
	}
	#endregion


	#region Input
	protected override void Update()
	{
		base.Update();

		LevelInput();

		PlayerInput();
	}


	/// <summary>
	/// Shortcuts to change current level.
	/// </summary>
	void LevelInput()
	{
		// Restart level.
		if (Input.GetKeyDown(KeyCode.R))
			level.Restart();
		// Next level.
		if (Input.GetKeyDown(KeyCode.N))
			level.NextLevel();
		// Previous level.
		if (Input.GetKeyDown(KeyCode.P))
			level.PreviousLevel();

	}


	/// <summary>
	/// Player movement input.
	/// </summary>
	void PlayerInput()
	{
		// Ignore input during the game over state.
		if (level.IsGameOver)
			return;

		if (Input.GetKeyDown(KeyCode.UpArrow))
			Move(0, 1);
		if (Input.GetKeyDown(KeyCode.DownArrow))
			Move(0, -1);
		if (Input.GetKeyDown(KeyCode.RightArrow))
			Move(1, 0);
		if (Input.GetKeyDown(KeyCode.LeftArrow))
			Move(-1, 0);

		// Victory (but not loading next level yet).
		if (AtExit)
			return;

		// Skip turn.
		if (Input.GetKeyDown(KeyCode.W))
		{
			// Minotaur walks.
			minotaur.TakeTurn();
			// Invoking event.
			InvokeStopEvent();
		}

		// Game over.
		if (minotaur.position == position)
			level.GameOver(true);
	}


	/// <summary>
	/// Called after the player stops.
	/// </summary>
	protected override void OnStop()
	{
		base.OnStop();

		// Victory.
		if (AtExit)
		{
			level.NextLevel(true);
			return;
		}

		// Minotaur waits for the player to stop.
		minotaur.TakeTurn();

		// Game over.
		if (minotaur.position == position)
			level.GameOver(true);

		// Invoking event.
		InvokeStopEvent();
	}
	#endregion
}
