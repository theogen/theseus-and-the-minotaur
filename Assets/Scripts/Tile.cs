// Script that is attached to a tile.

using UnityEngine;

public class Tile : MonoBehaviour
{
	#region Inspector
	public enum Wall { None, H, V, Corner }

	public Wall wall = Wall.None;
	#endregion


	#region Properties
	public bool Exit
	{
		get { return exit; }
		set
		{
			exit = value;
			transform.Find("exit").gameObject.SetActive(value);
		}
	}

	bool exit;
	#endregion


	#region Private fields
	GameObject wallH = null;
	GameObject wallV = null;

	Level level = null;
	#endregion


	void Start()
	{
		wallH = transform.Find("h").gameObject;
		wallV = transform.Find("v").gameObject;
		level = transform.parent.GetComponent<Level>();
		UpdateWall();
	}


	/// <summary>
	/// Enable wall sprites according to the enum value.
	/// </summary>
	public void UpdateWall()
	{
		if (wallH == null || wallV == null)
			return;
		switch (wall)
		{
			case Wall.None:
				wallH.SetActive(false);
				wallV.SetActive(false);
				break;
			case Wall.H:
				wallH.SetActive(true);
				wallV.SetActive(false);
				break;
			case Wall.V:
				wallH.SetActive(false);
				wallV.SetActive(true);
				break;
			case Wall.Corner:
				wallH.SetActive(true);
				wallV.SetActive(true);
				break;
		}
	}


	/// <summary>
	/// Called on inspector GUI change.
	/// Needed to easily design levels at runtime.
	///
	/// Unity will throw warnings because it doesn't like when you change
	/// objects inside OnValidate(), but they will never be in the final build.
	/// </summary>
	void OnValidate()
	{
		UpdateWall();
		level?.UpdateLevelData();
	}
}
