// Script that manages level and tile data.

using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
	#region Constants
	const float TileSize = 1f;
	const int FieldSize = 16;
	#endregion


	#region Inspector
	[System.Serializable]
	public struct LevelInfo
	{
		public string Commentary;
		public string LevelData;
		public Vector2Int PlayerPosition;
		public Vector2Int MinotaurPosition;
		public Vector2Int ExitPosition;
	}

	public GameObject TilePrefab;

	public Color TileColor;
	public Color TileOffsetColor;

	public Player Player;
	public Minotaur Minotaur;
	public UI UI;
	public History History;

	public string LevelData;

	public LevelInfo[] Levels;
	#endregion


	#region Properties
	public bool IsGameOver { get; private set; }

	public LevelInfo CurrentLevel
	{
		get { return Levels[level]; }
	}
	#endregion


	#region Private fields
	int level = 0;

	List<Tile> tiles = new List<Tile>();
	#endregion


	#region Initialization
	void Start()
	{
		SpawnTiles(FieldSize, FieldSize);
		LoadLevel(Levels[level]);
		History.OnUndo += OnUndo;
		UI.OnButtonClick += OnButtonClick;
	}
	#endregion


	#region Tiles
	/// <summary>
	/// Initial placement of level tiles.
	/// </summary>
	/// <param name="w"></param>
	/// <param name="h"></param>
	void SpawnTiles(int w, int h)
	{
		for (int y = 0; y < h; ++y)
		{
			for (int x = 0; x < w; ++x)
			{
				// Spawning a tile.
				GameObject tile = Instantiate(TilePrefab);
				tile.transform.position = new Vector2(x * TileSize, y * TileSize);
				tile.transform.parent = transform;
				tile.name = string.Format("{0}:{1}", x, y);
				tiles.Add(tile.GetComponent<Tile>());

				// Setting different color to offset tiles so the grid is visible.
				SpriteRenderer renderer = tile.GetComponent<SpriteRenderer>();
				bool isOffset = (x % 2 == 0 && y % 2 != 0) || (x % 2 != 0 && y % 2 == 0);
				renderer.color = TileColor;
				if (isOffset)
					renderer.color = TileOffsetColor;
			}
		}
	}


	/// <summary>
	/// Get tile by its position.
	/// </summary>
	/// <param name="x"></param>
	/// <param name="y"></param>
	/// <returns></returns>
	public Tile GetTile(int x, int y)
	{
		return tiles[y * FieldSize + x];
	}
	#endregion


	#region Game State
	/// <summary>
	/// Set game over state.
	/// </summary>
	/// <param name="state"></param>
	public void GameOver(bool state)
	{
		UI.SetGameoverText(state);
		IsGameOver = state;
	}


	/// <summary>
	/// Set victory state.
	/// </summary>
	/// <param name="state"></param>
	public void Victory(bool state)
	{
		UI.SetVictoryText(state);
		// Not using another variable because it serves the same purpose
		IsGameOver = state;
	}
	#endregion


	#region Level State
	/// <summary>
	/// Reset current level.
	/// </summary>
	public void Restart()
	{
		LoadLevel(Levels[level], false);
	}


	/// <summary>
	/// Load previous level.
	/// </summary>
	public void PreviousLevel()
	{
		if (level - 1 >= 0)
		{
			level -= 1;
			LoadLevel(Levels[level]);
		}
	}


	/// <summary>
	/// Load next level.
	/// </summary>
	public void NextLevel(bool showVictory = false)
	{
		if (Levels.Length > level + 1)
		{
			level += 1;
			LoadLevel(Levels[level]);
		}
		else if (showVictory)
		{
			Victory(true);
		}
	}


	/// <summary>
	/// Load level data and characters position.
	/// </summary>
	/// <param name="info">Level to load.</param>
	/// <param name="loadData">Whether to load the level data or just set player positions.</param>
	public void LoadLevel(LevelInfo info, bool loadData = true)
	{
		if (loadData)
			LoadFromLevelData(info.LevelData);
		// Reset player and minotaur positions.
		Player.SetPosition(info.PlayerPosition);
		Player.StopMovement();
		Minotaur.SetPosition(info.MinotaurPosition);
		Minotaur.StopMovement();
		// Set exit tile.
		GetTile(info.ExitPosition.x, info.ExitPosition.y).Exit = true;
		// Update UI text.
		UI.SetLevelNumber(level + 1);
		UI.SetLevelCommentary(info.Commentary);
		UI.SetGameoverText(false);
		UI.SetVictoryText(false);
		IsGameOver = false;
		// Clear history.
		History.Wipe();
		History.Record();
	}
	#endregion


	#region Callbacks
	/// <summary>
	/// Called from UI.
	/// </summary>
	/// <param name="name">Button name.</param>
	void OnButtonClick(string name)
	{
		if (name == "wait")
		{
			Minotaur.TakeTurn();
			History.Record();
		}
		if (name == "undo")
			History.Undo();
		if (name == "restart")
			Restart();
		if (name == "previous")
			PreviousLevel();
		if (name == "next")
			NextLevel();
	}

	/// <summary>
	/// Called from History.
	/// </summary>
	void OnUndo()
	{
		GameOver(false);
		Victory(false);
	}
	#endregion


	#region Level Data
	/// <summary>
	/// Deserializes/writes current level data to the LevelData field.
	/// </summary>
	public void UpdateLevelData()
	{
		LevelData = "";
		foreach (Tile tile in tiles)
		{
			if (tile.wall == Tile.Wall.None)
				continue; // Ignore empty tiles
			LevelData += string.Format("{0},{1};", tile.name, (int)tile.wall);
		}
	}


	/// <summary>
	/// Loads level data from a string.
	/// </summary>
	/// <param name="levelData">Serialized level data.</param>
	void LoadFromLevelData(string levelData)
	{
		foreach (Tile tile in tiles)
		{
			tile.wall = Tile.Wall.None;
			tile.Exit = false;
			tile.UpdateWall();
		}
		foreach (string tile in levelData.Split(';'))
		{
			if (tile.Length == 0)
				continue;
			string[] fields = tile.Split(',');
			string name = fields[0];
			string wall = fields[1];
			Tile script = transform.Find(name).GetComponent<Tile>();
			script.wall = (Tile.Wall)System.Convert.ToInt32(wall);
			script.UpdateWall();
		}
	}
	#endregion
}
