// Base class for both the player and the minotaur.

using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
	#region Events
	public delegate void VoidNoArg();
	public event VoidNoArg OnStopEvent;
	#endregion


	#region Inspector
	public float Speed = 6f;
	#endregion


	#region Properties
	public Vector2Int position { get; protected set; }
	#endregion


	#region Private fields
	protected Level level;

	/// <summary>
	/// Whether the moving animation is being played.
	/// </summary>
	bool moving = false;

	/// <summary>
	/// Minotaur moves in 2 steps, so a moving queue is needed.
	/// </summary>
	List<Vector2Int> moveQueue = new List<Vector2Int>();
	#endregion


	#region Initialization
	void Start()
	{
		level = GameObject.Find("level").GetComponent<Level>();
	}

	// Set character's position on the grid.
	public void SetPosition(Vector2Int pos)
	{
		position = pos;
		transform.position = new Vector2(pos.x, pos.y);
	}
	#endregion


	#region Movement
	/// <summary>
	/// Move the character, if possible.
	/// </summary>
	/// <param name="dir">Direction to move towards.</param>
	/// <returns></returns>
	public bool Move(Vector2Int dir)
	{
		bool canMove = CanMove(dir);
		if (canMove)
		{
			moving = true;
			position = position + dir;
			moveQueue.Add(position);
		}
		return canMove;
	}

	/// <summary>
	/// And overload for the Move() method that takes integers directly.
	/// </summary>
	/// <param name="xdir">X direction.</param>
	/// <param name="ydir">Y direction.</param>
	/// <returns></returns>
	public bool Move(int xdir, int ydir)
	{
		return Move(new Vector2Int(xdir, ydir));
	}


	/// <summary>
	/// Checks whether the character can move in a given direction.
	/// </summary>
	/// <param name="dir"></param>
	/// <returns></returns>
	public bool CanMove(Vector2Int dir)
	{
		Vector2Int target = position + dir;
		//Debug.Log(target);
		if (dir.x > 0 && IsHorizontalWall(target))   // Right
			return false;
		if (dir.x < 0 && IsHorizontalWall(position)) // Left
			return false;
		if (dir.y > 0 && IsVerticalWall(position))   // Up
			return false;
		if (dir.y < 0 && IsVerticalWall(target))     // Down
			return false;
		return true;
	}


	/// <summary>
	/// Check if a tile at a given position has a horizontal wall.
	/// </summary>
	/// <param name="pos"></param>
	/// <returns></returns>
	private bool IsHorizontalWall(Vector2Int pos)
	{
		Tile tile = level.GetTile(pos.x, pos.y);
		return tile.wall == Tile.Wall.V || tile.wall == Tile.Wall.Corner;
	}


	/// <summary>
	/// Check if a tile at a given position has a vertical wall.
	/// </summary>
	/// <param name="pos"></param>
	/// <returns></returns>
	private bool IsVerticalWall(Vector2Int pos)
	{
		Tile tile = level.GetTile(pos.x, pos.y);
		return tile.wall == Tile.Wall.H || tile.wall == Tile.Wall.Corner;
	}

	protected virtual void Update()
	{
		if (moving)
		{
			// Target position.
			var moveTowards = new Vector2(moveQueue[0].x, moveQueue[0].y);
			// Smooth movement towards a target in the move queue.
			transform.position = Vector2.MoveTowards(transform.position, moveTowards, Time.deltaTime * Speed);
			// Using a treshold to determine if the character has arrived at the destination.
			if (Vector3.Distance(transform.position, moveTowards) < 0.01)
			{
				transform.position = moveTowards;
				moveQueue.RemoveAt(0);
				// Only when the queue is empty the character is considered as not moving.
				if (moveQueue.Count == 0)
				{
					moving = false;
					OnStop();
				}
			}
		}
	}

	/// <summary>
	/// Stop all movement immediately.
	/// </summary>
	public void StopMovement()
	{
		moving = false;
		transform.position = new Vector2(position.x, position.y);
		moveQueue.Clear();
	}


	/// <summary>
	/// Called on the frame at which character stopped.
	/// </summary>
	protected virtual void OnStop()
	{
	}

	protected void InvokeStopEvent()
	{
		OnStopEvent?.Invoke();
	}
	#endregion
}
