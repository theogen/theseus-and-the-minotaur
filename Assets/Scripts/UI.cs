// Game UI helper.

using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
	#region Events
	public delegate void ButtonClick(string name);
	public event ButtonClick OnButtonClick;
	#endregion


	#region Private fields
	GameObject gameOver;
	GameObject victory;
	Text levelNumber;
	Text levelCommentary;
	#endregion


	#region Text/UI state
	private void Start()
	{
		gameOver = transform.Find("bottom/gameover").gameObject;
		victory = transform.Find("bottom/victory").gameObject;
		levelNumber = transform.Find("top/level_text").GetComponent<Text>();
		levelCommentary = transform.Find("top/comment").GetComponent<Text>();
	}

	public void SetGameoverText(bool state)
	{
		gameOver.SetActive(state);
	}

	public void SetVictoryText(bool state)
	{
		victory.SetActive(state);
	}

	public void SetLevelNumber(int number)
	{
		levelNumber.text = string.Format("Level {0}", number);
	}

	public void SetLevelCommentary(string text)
	{
		levelCommentary.text = text;
	}
	#endregion


	#region Callbacks
	/// <summary>
	/// Called on any button click.
	/// </summary>
	/// <param name="name">Button name.</param>
	public void _OnButtonClick(string name)
	{
		OnButtonClick?.Invoke(name);
	}
	#endregion
}
