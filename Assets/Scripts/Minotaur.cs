// Minotaur AI.

using UnityEngine;

public class Minotaur : Character
{
	#region Inspector
	public Player player;
	#endregion


	#region Movement
	/// <summary>
	/// Called after player moves or waits.
	/// </summary>
	public void TakeTurn()
	{
		Step();
		Step();
	}

	/// <summary>
	/// A single step, Minotaur can take 2.
	/// </summary>
	private void Step()
	{
		float right = GetPlayerDistance(1, 0);
		float left = GetPlayerDistance(-1, 0);

		if (right < left)
			if (Move(1, 0))
				return;
		if (left < right)
			if (Move(-1, 0))
				return;

		float up = GetPlayerDistance(0, 1);
		float down = GetPlayerDistance(0, -1);

		if (up < down)
			if (Move(0, 1))
				return;
		if (down < up)
			if (Move(0, -1))
				return;
	}

	/// <summary>
	/// A distance to player if the minotaur would step to a given direction.
	/// </summary>
	/// <param name="xdir">Direction X.</param>
	/// <param name="ydir">Direction Y.</param>
	/// <returns></returns>
	public float GetPlayerDistance(int xdir, int ydir)
	{
		return Vector2Int.Distance(player.position, position + new Vector2Int(xdir, ydir));
	}
	#endregion
}
